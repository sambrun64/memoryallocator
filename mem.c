/* On inclut l'interface publique */
#include "mem.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/* Définition de l'alignement recherché
 * Avec gcc, on peut utiliser __BIGGEST_ALIGNMENT__
 * sinon, on utilise 16 qui conviendra aux plateformes qu'on cible
 */
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

/* Cette constante permet de définir a la compilation (via make DEBUGP=1) une macro définissant le type d'affichage souhaité.
 * Quand elle est définie, une affichage verbeux est en place
 */
#ifndef DEBUGP
#define PRGDEBUG 0
#else
#define PRGDEBUG 1
#endif

/* permet de définir a la compilation la méthode de recherche des zones libres :
 * make --> recherche par défaut avec mem_fit_first
 * make MEMFIT=1 --> recherche utilisant mem_fit_best
 * make MEMFIT=2 --> recherche via mem_fit_worst
 */
#ifndef MEMFIT
#define MEMFIT 0
#endif

/* Macro définissant les couleurs utilisées pour l'affichage */
#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define NOCOLOR "\033[0m"

/* structure placée au début de la zone de l'allocateur

   Elle contient toutes les variables globales nécessaires au
   fonctionnement de l'allocateur

   Elle peut bien évidemment être complétée
*/
struct allocator_header {
    size_t memory_size;
	struct fb* tete; // définition de notre tete de liste de blocs libres
	mem_fit_function_t *fit; //fonction de choix des zones libres ???
};

/* La seule variable globale autorisée
 * On trouve à cette adresse le début de la zone à gérer
 * (et une structure 'struct allocator_header)
 */
static void* memory_addr;

static inline void *get_system_memory_addr() {
	return memory_addr;
}

static inline struct allocator_header *get_header() {
	struct allocator_header *h;
	h = get_system_memory_addr();
	return h;
}

static inline size_t get_system_memory_size() {
	return get_header()->memory_size;
}

/* Structure chainée de gestion des zones libres */
struct fb {
	size_t size;
	struct fb* next;
};

/* Structure pour la gestion des zones occupées */
struct db {
	size_t size;
};

/* Info sur le type de zone pris en compte */
typedef enum info_Zone {LIBRE = 0, OCCUPEE = 1, INVALIDE = 3} infoZone;


/*
 * mem_init : 
 * Entrée : une adresse mémoire mem et une size_t taille 
 * Sortie : aucune
 * Spécification : initialise la mémoire de notre allocateur a l'adresse mem et d'une taille taille
 * 	 Pour cela, placer et remplir un allocateur_header au debut de la mémoire et une premiere zone libre 
 * 	 collée apres celui-ci. De plus définit le mem_fit a la compilation.
 */
void mem_init(void* mem, size_t taille){
	/* Indication du type d'affichage */
	if(PRGDEBUG)
		fprintf(stderr,"\n\n ====== Affichage en mode debug ====== \n\n");

	/* Actualisation de memory_addr a l'adresse initiale de note allocateur */
    memory_addr = mem;

	/* Ecriture du allocator_header au début de la mémoire : */
	/* vérification de l'espace necessaire : si il est impossible d'écrire au moins le header d'entète on quitte
	 * sans rien avoir alloué */
	if(taille < sizeof(struct allocator_header)){
		fprintf(stderr,RED"ERROR (mem_init) :"NOCOLOR" taille mémoire insufisante pour stocker allocator_header !\n");
		exit(1);
	}
	/* INFOS DE DEBUG */
	if(PRGDEBUG){
		fprintf(stderr,"Adresse allocateur : " GREEN "%p\n" NOCOLOR,mem);
		fprintf(stderr,"Header (taille : " GREEN "%lu"NOCOLOR"): adresse size : " GREEN "%p"NOCOLOR", size : " GREEN "%lu"NOCOLOR" \n",sizeof(struct allocator_header),get_header(),taille);
	}

	/* le premier champ de l'allocateur étant la taille on l'écrit a l'adresse de début de l'allocateur (on inclut la taille du header)*/
	get_header()->memory_size=taille;

	/* Ecriture du champ tete de l'header allocateur, ici on indique ou commence le chainage des zones  */
	get_header()->tete = memory_addr + sizeof(struct allocator_header);
	/* INFOS DE DEBUG */
	if(PRGDEBUG)
		fprintf(stderr,"Header : adresse tete : " GREEN "%p"NOCOLOR" (taille size_t : " GREEN "%lu"NOCOLOR")\n",&get_header()->tete,sizeof(size_t));
	
	/* Ecriture du fb de la première zone libre au tout début de la mémoire : */
	/* Verification de l'espace nécessaire pour écrire le header de la première cellule libre : si il est impossible de
	l'ecrire, la mémoire sera inutilisable donc on exit */
	if(taille < sizeof(struct allocator_header) + sizeof(struct fb)){
		fprintf(stderr,RED"ERROR (mem_init) :"NOCOLOR" taille mémoire insufisante pour stocker la permiere cellule libre !\n");
		exit(1);
	}

	/* Initialisation du premier fb : */
	/* sa taille est la taille donnée pour l'allocation totale - la taille des 2 header mis en place */
	get_header()->tete->size = taille - sizeof(struct allocator_header) - 8;
	/* Pour le moment il n'y a aucuns autre bloc libre */
	get_header()->tete->next = NULL;
	if(PRGDEBUG)
		fprintf(stderr,"Cellule 1 : adresse size : " GREEN "%p"NOCOLOR" , taille : " GREEN "%lu"NOCOLOR" , adresse next : " GREEN "%p"NOCOLOR" ,next : " GREEN "%p"NOCOLOR"\n",
			get_header()->tete,get_header()->tete->size,&get_header()->tete->next,get_header()->tete->next);
			
	/* Vérification des infos présentes dans le header et de leurs accéssibilitée */
	assert(mem == get_system_memory_addr());
	assert(taille == get_system_memory_size());

	/* définit MEMFIT en fonction de la valeur donnée a la compilation : Cf compte-rendu */
	switch (MEMFIT)
	{
	case 1: //make MEMFIT=1
		mem_fit(&mem_fit_best);
		break;
	case 2: //make MEMFIT=2
		mem_fit(&mem_fit_worst);
		break;
	default: //make
		mem_fit(&mem_fit_first);
		break;
	}
}

/*
 * mem_show :
 * Entrée : La fonction print dont les paramètres sont (void *, size_t, int) ou void * est l'adresse de la zone a afficher, 
 *          size_t sa taille et int un booléen qui vaut 1 si la zone est libre et 0 si elle est occupée
 * Sortie : aucune 
 * Spécification : Affiche l'état de la mémoire 
*/
//// ATTENTION CAS OU LA TETE EST NULL (ALLOCATEUR FULL)
void mem_show(void (*print)(void *, size_t, int)) {
	int est_libre;
	struct allocator_header * head = get_header();
	/* Taille totale de l'allocateur (headers inclus) */
	size_t total_size = head->memory_size;

	/* Adresse des blocs : */
	/* Premier bloc de la structure : position -> memory_addr + taille_header */
	void* bloc = memory_addr + sizeof(struct allocator_header);
	/* Premier bloc libre */
	void* cellule_libre = head->tete;

	/* taille des blocs deja affichés : en dessous de current_size, tous les blocs ont deja été affichés */
	size_t curent_size = sizeof(struct allocator_header);

	/* tant qu'on a pas parcouru toute la mémoire */
	while (curent_size < total_size) {
		/* Si le bloc courant et la cellule libre sont aux meme niveaux, la cellule est libre */
		if(cellule_libre == bloc){
			est_libre = 1;
			cellule_libre = ((struct fb*)cellule_libre)->next; // on consomme la cellule libre
		/* Sinon elle est occupée */
		}else{
			est_libre = 0;
		}
		
		/* affichage du bloc : */
		print(bloc, ((struct db*)bloc)->size, est_libre);
		
		/* Consommation du bloc : on se déplace de la taille comprise dans le bloc + la taille du header */
		/* est_libre*sizeof(struct fb) + (1-est_libre)*sizeof(struct db) : ajoute en fonction de est_libre la taille
		de la structure adaptée (8 ou 16)*/
		curent_size = curent_size + ((struct db*)bloc)->size + 8;
		bloc = memory_addr + curent_size;
	}
}

/*
mem_fit:
Entree: un pointeur sur fonction respectant le type mem_fit_function_t
Sortie: aucune
Specification: définit la fonction de fit de l'allocateur
*/
void mem_fit(mem_fit_function_t *f) {
	get_header()->fit = f;
}


int taille_reelle;
/*
 * trouve_pointe_zone :
 * Entree : Pointeur sur une zone libre 
 * Sortie : adresse de ce qui pointe sur une zone libre que l'on va allouer
 * Specification : Trouve qui pointe sur la zone a allouer
*/

struct fb** trouve_pointe_zone(struct fb* fb){
	struct fb** adr_pointeur_zone_a_allouer = &(get_header()->tete);
	while(adr_pointeur_zone_a_allouer != NULL && *adr_pointeur_zone_a_allouer != NULL && *adr_pointeur_zone_a_allouer != fb){
		adr_pointeur_zone_a_allouer = &((*adr_pointeur_zone_a_allouer)->next);
	}
	if(adr_pointeur_zone_a_allouer == NULL || *adr_pointeur_zone_a_allouer == NULL){
		if(PRGDEBUG)
			fprintf(stderr,"Problème de chainage des zones libres !\n");
		return NULL;
	}
	return adr_pointeur_zone_a_allouer;
}
/* 
 * mem_alloc :
 * Entrée : La taille de la zone a allouer
 * Sortie : Un pointeur vers la zone allouée ou bien NULL en cas d'echec
 * Spécification : Alloue une zone de taille donnée en paramètre si cela est possible (il y a donc une nouvelle zone occupée)
*/
void *mem_alloc(size_t taille) {

	/* Si l'utilisateur demande l'allocation d'une taille négative, cela échoue */
	if(taille<=0){
		if(PRGDEBUG)
			fprintf(stderr,"Impossible d'allouer une taille négative'!\n");
		return NULL;
	}

	__attribute__((unused)) /* juste pour que gcc compile ce squelette avec -Werror */	

	/* Cherche la zone libre a allouer en fonction du fit définit a l'initialisation */
	struct fb* fb=get_header()->fit(get_header()->tete, taille);
	
	/* CA VA BOUGER !!!!!!!!!!!!!!!!!!!!! */
	//////////// ATTENTION AU CALCUL DE TAILLE, taille doit etre = a la taille du bloc (taille demandé + alignement +padding) 
		////// sans pour autant le header
	taille = taille_reelle;

	/* cas ou aucune zone trouvée par le fit, echec */
	if(fb == NULL){
		if(PRGDEBUG)
			fprintf(stderr,"Impossible de trouver une zone libre!\n");
		return NULL;
	}

	/* Si la taille du bloc n'a pas la place de contenir les données demandés et un nouveau bloc libre */
	if(fb->size == taille){
		if(PRGDEBUG)
			fprintf(stderr,"size == taille\n");
		struct fb** adr_pointeur_zone = trouve_pointe_zone(fb);
		if(adr_pointeur_zone == NULL)
			return NULL;
		(*adr_pointeur_zone) = fb->next;
		return fb;
	}
	/* Verification de l'espace restant (dans le bloc libre qu'on alloue) afin de pouvoir mettre un header fb*/
	if(fb->size < taille + sizeof(struct fb)){
		if(PRGDEBUG)
			fprintf(stderr,"Pas assez d'espace pour stocker le nouvel header\n");
		return NULL;
	}

	/* calcul de l'adresse du nouveau bloc libre */
	void* nvl_adr_bloc =  ((void*)fb) + sizeof(struct db) + taille;
	struct fb* nv_bloc_libre = (struct fb*) nvl_adr_bloc;

	/* pour le calcul des liens de la liste chainée, trouver qui pointe sur fb */
	struct fb** adr_pointeur_zone_a_allouer = trouve_pointe_zone(fb);
	if(adr_pointeur_zone_a_allouer == NULL)
		return NULL;

	/* La taille du nouveau bloc = taille de l'ancien - taille allouée - taille du nouveau header - taille du pointeur fb (car
		ce dernier n'est plus utile dans le header de la zone qui vient d'etre allouée) */
	nv_bloc_libre->size = fb->size - taille -8;
	/* La zone libre suivant dans la liste est le nouveau bloc */
	*adr_pointeur_zone_a_allouer = nv_bloc_libre;
	/* Le suivant du nouveau bloc est le suivant du bloc qui vient d'etre alloué */
	nv_bloc_libre->next = fb->next;

	/* fb devient un db et la size est celle calculée plus haut */
	((struct db*)fb)->size = taille;
	if(PRGDEBUG)
		fprintf(stdout,"Zone allouée en %p (réelle en %p) d'une taille %lu cellule avant %p \n",nv_bloc_libre,fb,taille+sizeof(struct db),adr_pointeur_zone_a_allouer);
	return fb;
}



/* Utilise le meme principe que pour l'affichage pour determiner les zones libre et occupées */
infoZone verif_zone(void* zone){
	/* Récupération des blocs a comprer */
	void* cellule_libre = get_header()->tete;
	void* bloc = get_system_memory_addr() + sizeof(struct allocator_header);

	/* Récupération des tailles */
	size_t total_size = get_header()->memory_size;
	size_t curent_size = sizeof(struct allocator_header);

	/* tant que toute la mémoire n'a pas été parcourue (ou a défaut que la zone a vérifier n'a pas été trouvé) */
	while(curent_size < total_size){
		/* si la zone est libre */
		if(cellule_libre == zone){
			return LIBRE;
		}
		/* si la zone est occupée */
		if(bloc == zone){
			return OCCUPEE;
		}
		/* la zone courante est libre, on avance dans la chaine des zones libres */
		if(bloc == cellule_libre){
			cellule_libre = ((struct fb*)cellule_libre)->next;
			/* la différence de taille entre les headers libre / occ */
		}
		/* la nouvelle taille est l'ancienne + la taille du bloc qui vient d'etre parcourue + la taille du header */
		curent_size = curent_size + sizeof(struct db) + ((struct db*)bloc)->size;
		bloc = memory_addr + curent_size;
	}
	if(PRGDEBUG)
		fprintf(stdout,"Zone invalide en %p\n",zone);
	return INVALIDE;
}
 
/* trouve l'adresse du fb tq : adr(fb) < adr du bloc a liberer < adr(fb->next) */
struct fb** pointe_sur(void* adr){
	struct fb** cell = &(get_header()->tete);
	while(cell != NULL && !(cell<(struct fb**)adr && (*cell ==  NULL || *cell > (struct fb*)adr))){
		cell = (struct fb**)((void*)*cell + sizeof(size_t));
	}
	return cell;
}

/*
 * mem_free :
 * Entrée : Une adresse mémoire mem
 * Sortie : Aucune 
 * Spécification : Met a jour la liste des zones libres, libère l'espace aloué à l'adresse mémoire mem
*/
void mem_free(void* mem) {
	struct fb* adr_mem = mem;
	if(PRGDEBUG)
		fprintf(stdout,"Adresse a liberer : %p\n",adr_mem);
	/* En premier lieu, on vérifie que l'adresse donnée pour la libération est cohérente */
	switch(verif_zone(adr_mem)){
		case LIBRE:
			/* Si la zone est deja libre, rien a faire */
			return;
		case OCCUPEE:
			/* Si la zone est occupée : on libère */
			{
			/* Si la tete du header est null, il suffit de considerer cette zone comme libre */
			if(get_header()->tete == NULL){
				get_header()->tete = adr_mem;
				adr_mem->next=NULL;
				//adr_mem->size -= 8; // on enlève de l'espace disponible la taille du pointeur de cellule
				return;
			}

			/* On cherche l'adresse z d'un pointeur sur struct fb tel que z < adr_mem et (*z > adr_mem ou *z == NULL)*/
			struct fb** z = pointe_sur(adr_mem);
			/* si z n'existe pas on est dans un cas d'erreur, en effet, z==NULL revient au cas ci-dessus et dans les autres cas
			z est au moins la tete incluse dans le header */
			if(z == NULL){
				fprintf(stderr,"mem_free : erreur chainage !\n");
				exit(1);
			}
			/* cas ou z correspond a la tete dans le header_allocateur */
			if(*z == get_header()->tete){
				/* Si la zone a liberer est la première dans la mémoire : */
				if(mem==memory_addr + sizeof(struct allocator_header)){
					/* infos de debug */
					if(PRGDEBUG){
						fprintf(stdout,"\nlibération au début\n");
						fprintf(stdout,"adr suivante : %p  tete %p size %lu taille st%lu\n",(char*)adr_mem + adr_mem->size + sizeof(struct fb*),get_header()->tete,adr_mem->size,sizeof(struct fb));
					}
					/* Si la zone tout de suite deriere est libre */
					if(verif_zone(mem + adr_mem->size + sizeof(struct db)) == LIBRE ){
						if(PRGDEBUG)
							fprintf(stdout,"\navec zone suivante libre\n");
						/* récupération de la zone suivante */
						struct fb* zone_suivante = (struct fb*)(mem + adr_mem->size + sizeof(struct db)); // en partant de mem on décale de 8 puis de size
						/*la taille de la nouvelle zone = taille ancienne -8 (car db devient une fb) + taille de celle collée + taille du header */
						adr_mem->size +=  zone_suivante->size + sizeof(struct db); 
						adr_mem->next = zone_suivante->next;
					/* la zone tout de suite deriere n'est pas libre */
					}else{
						if(PRGDEBUG)
							fprintf(stdout,"\nsans zone suivante libre\n");
						/* la zone libre suivante est celle dans le header */
						adr_mem->next = get_header()->tete;
						/* la taille se réduit (passage de db a fb) */
						//adr_mem->size -= 8;
					}
					/* on met a jours les liens */
					get_header()->tete = adr_mem;
					return;
				}
				/* sinon le fait d'etre la tete n'importe pas */
			}

			/* infos debug */
			if(PRGDEBUG){
				fprintf(stdout,"calc %p adrmem %p size %lu\n\n",(char*)z + sizeof(struct fb*) + *((size_t*)((char*)z - sizeof(size_t))),adr_mem,*((size_t*)((char*)z - sizeof(size_t))));
				fprintf(stdout,"calc 2 %p adr %p size %lu\n\n",(char*)adr_mem + adr_mem->size + sizeof(struct fb),adr_mem,adr_mem->size+sizeof(struct fb));
			}

			/* si la zone d'avant est libre (revient a dire que la zone d'apres z est adr_mem) */
			if(adr_mem == (void*)z + *((size_t*)((void*)z - sizeof(size_t)))){
				/* la taille de la cellule contenant z augmente de  size + taille(struct db) */
				*((size_t*)((char*)z-sizeof(size_t))) += adr_mem->size +8;
				if(PRGDEBUG)
					fprintf(stdout,"\n zone avant libre \n");
				/* si la zone d'apres est libre */
				if((void*)adr_mem + adr_mem->size + sizeof(struct db) < memory_addr + get_system_memory_size() && 
					verif_zone((void*)adr_mem + adr_mem->size + sizeof(struct db)) == LIBRE){
					/* la taille de la cellule contenant z augmente encore */
					*((size_t*)((char*)z-sizeof(size_t))) +=  sizeof(struct db) + (*z)->size;
					*z = (*z)->next;
				}
			}
			/* si seulement la zone d'apres est libre */
			else if((void*)((char*)adr_mem + adr_mem->size + sizeof(struct db)) < memory_addr + get_system_memory_size() && 
					verif_zone((char*)adr_mem + adr_mem->size + sizeof(struct db)) == LIBRE){
				/* on passe de db a fb donc on enlève 8 a la taille */
				adr_mem->size +=  sizeof(struct db) + (*z)->size;
				adr_mem->next = (*z)->next;
				*z=adr_mem;
				if(PRGDEBUG)
					fprintf(stdout,"\nadresse apres libre seul\n");
			}
			//si aucune zone autour est libre
			else{
				adr_mem->next = *z;
				*z = adr_mem;
				if(PRGDEBUG)
					fprintf(stdout,"\nAucune zone libre autour\n");
			}
			}
			break;
		default:
			/* Si la zone est ni libre ni occupée, erreur on ne fait rien */
			if(PRGDEBUG)
				fprintf(stderr,"mem_free : erreur adresse mem incorrecte !\n");
	}
}


int padding(int taille){
	return (taille + (ALIGNMENT - 1)) & ~(ALIGNMENT -1);
}
/*
 * mem_fit_first :
 * Entree : Adresse de la liste des blocs libres list et une taille size 
 * Sortie : Une adresse
 * Specification : Renvoie le premier bloc libre dans la liste des blocs libres dont la taille est 
 *                 supérieure ou egale a la taille donnee en parametre
*/
struct fb* mem_fit_first(struct fb *list, size_t size) {
	struct fb* cellule_libre = list;
	taille_reelle = padding(size+8) - 8;
	if(PRGDEBUG)
		fprintf(stderr,"Padding : %d pour alignemment %d a partir de %p\n",taille_reelle,ALIGNMENT,list);
	while(cellule_libre != NULL){
		//Pour éviter les trous dans la mémoire soit on demande pile la taille libre soit on demande une taille et on peut placer un header
		if(cellule_libre->size == taille_reelle){
			if(PRGDEBUG)
				fprintf(stderr,"cellule de taille pile\n");
			return cellule_libre;
		}
		if(cellule_libre->size >= taille_reelle + sizeof(struct fb)){
			if(PRGDEBUG)
				fprintf(stderr,"cellule pouvant contenir zone et header\n");
			return cellule_libre;
		}
		if(cellule_libre->size > taille_reelle){
			if(PRGDEBUG)
				fprintf(stderr,"cellule complété\n");
			taille_reelle += 8;
			return cellule_libre;
		}
		cellule_libre = cellule_libre->next;
	}
	return NULL;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
 
/*
 * mem_get_size 
 * Entree : L'adresse d'une zone allouee
 * Sortie : Une taille
 * Specification : Cette fonction renvoie le nombre d'octets maximum que l'utilisateur peut stocker dans le bloc
*/
size_t mem_get_size(void *zone) {
	return ((struct fb*)zone)->size;
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */

/*
 * mem_fit_best
 * Entree : L'adresse de la liste de blocs libres (list) et la taille souhaitée en bloc libre (size)
 * Sortie : Adresse d'un bloc memoire
 * Specification : Cette fonction renvoie l'adresse du premier bloc libre de taille supérieure ou égale à size, donne en entree.
*/

////////////// ATTENTION : AJOUTER LE CALCUL D'OFFSET TAILLE
struct fb* mem_fit_best(struct fb *list, size_t size) {
	struct fb* cellule_libre = list;
	struct fb* cellule_min = list;
	taille_reelle = padding(size+8) - 8;
	if(cellule_min == NULL){
		return NULL;
	}
	while(cellule_libre != NULL){
		//Pour éviter les trous dans la mémoire soit on demande pile la taille libre soit on demande une taille et on peut placer un header
		//on a une sol minimale donc on sort direct
		if(cellule_libre->size == taille_reelle){
			return cellule_libre;
		}
		if(cellule_libre->size >= taille_reelle && cellule_min->size > cellule_libre->size){
			cellule_min = cellule_libre;
		}
		cellule_libre = cellule_libre->next;
	}
	if(cellule_min->size > taille_reelle && cellule_min->size < taille_reelle + sizeof(struct fb)){
		taille_reelle +=8;
		return cellule_min;
	}
	if(cellule_min->size > taille_reelle){
		return cellule_min;
	}
	return NULL;
}


/*
 * mem_fit_worst
 * Entree : L'adresse de la liste de blocs libres (list) et la taille souhaitée en bloc libre (size)
 * Sortie : Adresse d'un bloc memoire
 * Specification : Cette fonction renvoie l'adresse du plus grand bloc libre de taille supérieure ou égale à size qui est donne en entree.
*/
struct fb* mem_fit_worst(struct fb *list, size_t size) {
	struct fb* cellule_libre = list;
	struct fb* cellule_max = list;
	taille_reelle = padding(size+8) - 8;
	if(cellule_max == NULL){
		return NULL;
	}
	while(cellule_libre != NULL){
		//Pour éviter les trous dans la mémoire soit on demande pile la taille libre soit on demande une taille et on peut placer un header
		if(cellule_libre->size >= taille_reelle && cellule_max->size < cellule_libre->size){
			cellule_max = cellule_libre;
		}
		cellule_libre = cellule_libre->next;
	}
	//si on a pas remplie la cond d'etre au moins de taille size on sort
	if(cellule_max->size == taille_reelle || cellule_max->size >= taille_reelle + sizeof(struct fb)){
		return cellule_max;
	}
	if(cellule_max->size > taille_reelle && cellule_max->size < taille_reelle + sizeof(struct fb)){
		taille_reelle +=8;
		return cellule_max;
	}
	
	return NULL;
}
