#include "tests.h"

void afficher_zone(void *adresse, size_t taille, int free)
{
  printf("Zone %s, Adresse : %lu, Taille : %lu\n", free?"libre":"occupee",
         adresse - get_memory_adr(), (unsigned long) taille);
}

void verif_retour_etat_init(){
    void* adr;
    if((int)((adr=mem_alloc(get_memory_size()-24-8))-get_memory_adr())!=24){
        fprintf(stderr,RED"\ntest_free Echec :"NOCOLOR" Retour etat initial memoire non correct !\n");
		exit(1);
    }
    mem_free(adr);
}

void* alloue_zone(int taille){
    void* adr;
    if((adr=mem_alloc(taille))==NULL){
        fprintf(stderr,RED"\ntest_free Echec :"NOCOLOR" allocation impossible de taille %d !",taille);
		exit(1);
    }

    return adr;
}

int main(int argc, char *argv[]) {
	fprintf(stderr, BLUE"test_free :"NOCOLOR" Tests de libération : ");
    srand( time( NULL ) );

    mem_init(get_memory_adr(),get_memory_size());
    char* adr[10];

    //free au debut avec 1 seul occ de taille mem
    adr[0]=alloue_zone(get_memory_size()-24-8);
    mem_free(adr[0]);
    verif_retour_etat_init();

    //free au debut avec plusieurs occ
    adr[0] = alloue_zone(100);
    adr[1] = alloue_zone(1000);
    adr[2] = alloue_zone(150);
    mem_free(adr[0]);
    adr[0] = alloue_zone(100);
    if(adr[0]==NULL){
        fprintf(stderr,RED"\ntest_free Echec :"NOCOLOR" allocation impossible au debut !");
		exit(1);
    }
    mem_free(adr[0]);
    mem_free(adr[1]);
    mem_free(adr[2]);
    verif_retour_etat_init();

    //free au debut avec zone libre derriere
    adr[0] = alloue_zone(100);
    adr[1] = alloue_zone(1000);
    adr[2] = alloue_zone(150);
    mem_free(adr[1]);
    mem_free(adr[0]);
    mem_free(adr[2]);
    verif_retour_etat_init();

    //free au centre avec aucune autre libre
    adr[0]=alloue_zone(get_memory_size()-24-16 -116);
    adr[1]=alloue_zone(100);
    mem_free(adr[1]);
    mem_free(adr[0]);
    verif_retour_etat_init();


    //free au centre avec libre a la fin 
    adr[0] = alloue_zone(100);
    adr[1] = alloue_zone(1000);
    adr[2] = alloue_zone(150);
    mem_free(adr[0]);
    mem_free(adr[2]);
    mem_free(adr[1]);
    verif_retour_etat_init();

    //free a la fin avec libre fin
    adr[0] = alloue_zone(100);
    adr[1] = alloue_zone(1000);
    adr[2] = alloue_zone(150);
    mem_free(adr[2]);
    mem_free(adr[1]);
    mem_free(adr[0]);
    verif_retour_etat_init();

    //free fin avec libre au centre
    adr[0] = alloue_zone(100);
    adr[1] = alloue_zone(1000);
    adr[2] = alloue_zone(150);
    mem_free(adr[1]);
    mem_free(adr[2]);
    mem_free(adr[0]);
    verif_retour_etat_init();

    //free zone libre
    adr[0] = alloue_zone(100);
    mem_free(adr[0]);
    mem_free(adr[0]);
    verif_retour_etat_init();

    //free zone non libre/occ
    mem_free(0);
    verif_retour_etat_init();

    //free taille 0
    adr[0] = alloue_zone(1);
    mem_free(adr[0]);
    verif_retour_etat_init();

    fprintf(stderr, GREEN"TEST PASSED\n"NOCOLOR);
	return 0;
}
