#include "tests.h"

void afficher_zone(void *adresse, size_t taille, int free)
{
  printf("Zone %s, Adresse : %lu, Taille : %lu\n", free?"libre":"occupee",
         adresse - get_memory_adr(), (unsigned long) taille);
}

int main(int argc, char *argv[]) {
	fprintf(stderr, BLUE"test_init2 :"NOCOLOR" Tests sur l'initialisation : ");

	//test allocation blocs de taille min
	mem_init(get_memory_adr(),40);
	if(mem_alloc(1) == NULL){
		fprintf(stderr,RED"\ntest_init2 Echec :"NOCOLOR" allocation impossible de taille 0 !");
		exit(1);
	}
	if(mem_alloc(1) != NULL){
		fprintf(stderr,RED"\ntest_init2 Echec :"NOCOLOR" allocation impossible de taille 1 !");
		exit(1);
	}
	mem_init(get_memory_adr(),40);
	if(mem_alloc(10) != NULL){
		fprintf(stderr,RED"\ntest_init2 Echec :"NOCOLOR" reallocation impossible de taille 1 !");
		exit(1);
	}

	//test initialisation ecrase bien le contenu de la mémoire :
	mem_init(get_memory_adr(),get_memory_size());
	void* adr1 = mem_alloc(10);
	void* adr2 = mem_alloc(10);
	void* adr3 = mem_alloc(get_memory_size() - (24 + 48 +8));
	if(!adr3 || !adr2 || !adr1){
		fprintf(stderr,RED"\ntest_init2 Echec :"NOCOLOR" allocation impossible !");
		exit(1);
	}
	//mem_show(&afficher_zone);
	if(mem_alloc(1) != NULL){
		fprintf(stderr,RED"\ntest_init2 Echec :"NOCOLOR" allocation possible alors qu'impossible!");
		exit(1);
	}
	mem_init(get_memory_adr(),104);
	if(mem_alloc(72) == NULL){
		fprintf(stderr,RED"\ntest_init2 Echec :"NOCOLOR" allocation impossible apres ecrasement !");
		exit(1);
	}
	if(mem_alloc(1) != NULL){
		fprintf(stderr,RED"\ntest_init2 Echec :"NOCOLOR" allocation possible alors qu'impossible apres ecrasement !");
		exit(1);
	}

	fprintf(stderr, GREEN"TEST PASSED\n"NOCOLOR);

	return 0;
}
