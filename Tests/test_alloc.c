#include "tests.h"

void afficher_zone(void *adresse, size_t taille, int free)
{
  printf("Zone %s, Adresse : %lu, Taille : %lu\n", free?"libre":"occupee",
         adresse - get_memory_adr(), (unsigned long) taille);
}

void verif_retour_init(){
    void* adr;
    if((int)((adr=mem_alloc(get_memory_size()-24-8))-get_memory_adr())!=24){
        fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" allocation impossible de taille max en 24 !\n");
		exit(1);
    }
    //mem_show(&afficher_zone);
    mem_free(adr);
}


int elem_a_free(char* tab,int taille){
    int vide = 0;
    for(int i=0;i<taille;i++){
        vide = vide | tab[i];
    }
    if(!vide)
        return -1;
    int rdm;
    while(1){
        rdm = rand()%taille;
        if(tab[rdm])
            return rdm;
    }
    return 0;
}

int main(int argc, char *argv[]) {
	fprintf(stderr, BLUE"test_alloc :"NOCOLOR" Tests d'allocation : ");
    srand( time( NULL ) );
    
    void* adr;
    //allocation taille 0
    mem_init(get_memory_adr(),get_memory_size());
    if(mem_alloc(0)!=NULL){
        fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" allocation impossible de taille 0 en 24 !\n");
		exit(1);
    }

    //allocation taille max
    if((int)((adr=mem_alloc(get_memory_size()-24-8))-get_memory_adr())!=24){
        fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" allocation impossible de taille max en 24 !\n");
		exit(1);
    }
    mem_free(adr);

    int i;
    //20 allocation puis free immédiat 
    for(i=0;i<20;i++){
        if((int)((adr=mem_alloc(10))-get_memory_adr())!=24){
            fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" allocation/liberation a la suite !\n");
		    exit(1);
        }
        mem_free(adr);
    }
    //verif si on est bien revenu dans etat init
    verif_retour_init();

    //test padding : 
    void* adr1=mem_alloc(1);
    if((adr=mem_alloc(1))-get_memory_adr()!=40){
        fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" padding incorrect !\n");
		exit(1);
    }
    mem_free(adr1);
    mem_free(adr);

    char* adr_tab[20];
    //20 allocation puis free dans le meme ordre 
    for(i=0;i<20;i++){
        adr_tab[i]=(char*)mem_alloc(8);
        if((int)((void*)adr_tab[i]-get_memory_adr())!=24+i*16){
            fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" allocation liberation meme odre !\n");
		    exit(1);
        }
    }
    for(i=0;i<20;i++){
        mem_free(adr_tab[i]);
    }
    verif_retour_init();

    //20 allocation puis free dans l'ordre inverse  
    for(i=0;i<20;i++){
        adr_tab[i]=(char*)mem_alloc(8);
        if((int)((void*)adr_tab[i]-get_memory_adr())!=24+i*16){
            fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" allocation et liberation inverse !\n");
		    exit(1);
        }
    }
    for(i=19;i>=0;i--){
        mem_free(adr_tab[i]);
    }
    verif_retour_init();

    
    //nb allocation puis free dans un ordre hasard
    int nb = 10;
    for(i=0;i<nb;i++){
        adr_tab[i]=(char*)mem_alloc(8);
        if((int)((void*)adr_tab[i]-get_memory_adr())!=24+i*16){
            fprintf(stderr,RED"\ntest_alloc Echec :"NOCOLOR" allocation puis liberation alea !\n");
		    exit(1);
        }
    }
    int nlib=0;
    int alea;
    while(nlib<nb){
        alea = rand()%nb;
        if(adr_tab[alea]!=NULL){
            //mem_show(afficher_zone);
            //printf("\na lib : %ld\n",(void*)adr_tab[alea]-get_memory_adr());
            mem_free(adr_tab[alea]);
            nlib++;
            adr_tab[alea]=NULL;
        }
    }
    verif_retour_init();

    //allocation et free au hasard
    int nb_op = 50;
    char* adr_a_lib[50];
    char tab_adr_lib[50];
    //utile pour le test suivant
    int tab_taille_all[50];

    for(i=0;i<nb_op;i++){
        tab_adr_lib[i]=0;
    }
    int rdm;
    i=0;
    int indice=0;
    while(i<nb_op || indice!=-1){
        rdm = rand()%2;
        if(rdm && i<nb_op){
            adr_a_lib[i] = (char*)mem_alloc(20);
            if(adr_a_lib[i] != NULL){
                tab_adr_lib[i]=1;
                indice =0;
            }
        }else{
            indice = elem_a_free(tab_adr_lib,nb_op);
            if(indice!=-1){
                mem_free(adr_a_lib[indice]);
                tab_adr_lib[indice]=0;
            }
        }
        i++;
    }
    verif_retour_init();

    //allocation et free au hasard taille random
    int rdmt;
    int tailleAll;

    int nb_all_lib = 100;
    for(int j=1;j<nb_all_lib;j++){
        for(i=0;i<nb_op;i++){
            tab_adr_lib[i]=0;
            tab_taille_all[i]=0;
        }
        tailleAll = 0;
        i=0;
        indice=0;
        while(i<nb_op || indice!=-1){
            rdm = rand()%2;
            if(rdm && i<nb_op){
                rdmt = rand()%(1500)+1;
                adr_a_lib[i] = (char*)mem_alloc(rdmt);
                if(adr_a_lib[i] != NULL){
                    tab_adr_lib[i]=1;
                    indice =0;
                    tailleAll += rdmt;
                    tab_taille_all[i]= rdmt;
                }
            }else{
                indice = elem_a_free(tab_adr_lib,nb_op);
                if(indice!=-1){
                    mem_free(adr_a_lib[indice]);
                    tab_adr_lib[indice]=0;
                    tailleAll -= tab_taille_all[indice];
                    tab_taille_all[indice]=0;
                }
            }
            //mem_show(&afficher_zone);
            //printf("\n");
            i++;
        }
        verif_retour_init();
    }

    fprintf(stderr, GREEN"TEST PASSED\n"NOCOLOR);
	return 0;
}
