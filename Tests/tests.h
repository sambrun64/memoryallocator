#ifndef __TESTS_H
#define __TESTS_H

#include "../mem.h"
#include "../common.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

#endif