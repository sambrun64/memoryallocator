# Compte-rendu TP Allocateur
## AVANTURIER Aurélie / BRUN Samuel

Choix d'implémentations
------

Tout d'abord, notre code utilise les structures :
```c
struct allocator_header {
    size_t memory_size;
	struct fb* tete; // définition de notre tete de liste de blocs libres
	mem_fit_function_t *fit; //fonction de choix des zones
};
/* Structure chainée de gestion des zones libres */
struct fb {
	size_t size;
	struct fb* next;
};

/* Structure pour la gestion des zones occupées */
struct db {
	size_t size;
};
```
*Structures de l'allocateur*

La structure allocator_header représente et conserve des informations sur la structure générale de l'allocateur, tel qu'un pointeur sur une struct fb. Les structs fb sont une liste chainée de zones libres, il est possible qu'il n'y ait plus de zones libres. Les struct db correspondent aux zones occupées.

*Contenu des cellules libres*

La size contenue dans le champ size des struct fb représente la taille du bloc qui est en train d'etre analysée, cette taille est la taille disponible à l'allocation, c'est a dire la taille réllement libre + l'espace du struct fb* qui n'a pas lieu d'etre dans les struct db. Mettre la taille disponible a l'allocation permet d'éviter de nombreuses convertions de taille de cellule au moment des passages de libre a occupée ou l'inverse.
(CF. [annexe](Annexe_CR-AVANTURIER-BRUN.jpg) figure 1)

*Chainage des zones libres*

Concernant le chainage des zones libres, il s'effectue de facon croissante, c'est à dire qu'a aucun moment l'adresse de la zone suivante ne sera avant l'adresse de la cellule elle-meme. Cela a pour interet de faciliter la fusion lors des libérations puisqu'il suffit de trouver les zones d'avant et d'apres et des vérifiés pour potenetiellement les fusionner. 
(CF. [annexe](Annexe_CR-AVANTURIER-BRUN.jpg) figure 2)

*Deux mots sur le padding et l'alignement*

Afin de conserver une mémoire cohérente (qui ne possède pas de trous dans les blocs), il faut ajouter un mécanisme de padding. Ainsi quand la taille demandée par l'utilisateur a l'allocation ne permet pas de remplir parfaitement la cellule libre à allouer ou qu'elle ne permet pas de placer une nouvelle fb a la suite dans la zone allouée. Il ne faut pas creer de trou, on va donc ajouter des octets a la taille utilisable pour remplir la zone. (cf. annexe figure 3)

L'alignement est quant à lui necessaire pour garantir une efficacité d'acces aux inforamtions de la structure. Dans notre allocateur un alignement des blocs (headers inclus) est effectué. (cf. [annexe](Annexe_CR-AVANTURIER-BRUN.jpg) figure 4)

Fonctionnalités
------

*Fit mémoire*

La premiere fonctionnalitée est celle du fit mémoire.
Lors de la compilation, il est possible de choisir la fonction de recherche des zones libre de la facon suivante :
* make //fit first
* make MEMFIT=1 // fit best
* make MEMFIT=2 // fit worst

La politique fit first est utile car elle permet un gain en temps. En effet, contrairement aux deux autres, elle ne necessite pas le parcours de la mémoire en entier, elle s'arréte lorsqu'une zone pouvant contenir la taille demandée est trouvée. L'inconvéignant de cette méthode est que la taille des zones restante n'est pas optimisée.

La politique fit best permet un parcours complet de la mémoire afin de trouver la plus petite zone pouvant contenir la taille souhaitée. Cela à pour effet de maximiser l'espace mémoire restant afin de creer les zones libres les plus petites possibles.

Enfin la politique fit worst n'est ni rentable en temps (car parcourt toute la mémoire) ni en espace (car minimise la taille des blocs libres). Elle a créé beaucoup de petits blocs libres dans la mémoire. Avec cette méthode l'allocation de gros blocs mémoire devient compliquée.

*Debug*

Avec la commande suivante à la compilation, on peut activer un affichage verbeux :
* make DEBUGP=1 

Cette affichage nous a notamment été utile pour débugger et tester notre implémentation. Elle affiche pleins d'informations sur l'état de la mémoire et les opérations effectuées.

Tests
------

Nous avons fait 3 fichiers de tests automatiques :
* test_init2 : complète les tests d'initialisation avec des écrasements de mémoire ou encore des allocations de taille réduite.
* test_alloc : réalise des tests complets d'allocation, dont des tests aux bornes (avec des tailles max et min), des tests de padding, d'alignement ou encore de multiples allocations/libérations aléatoires.
* test_free : test chacun des cas de libération mémoire et de fusion mémoire des zones libres.

En complément, nous avons effectué de multiples tests à la main via mem_sell pour vérifier mem_show, et compléter les tests des autres fonctions.

Limites
-----

Les limites de notre allocateur sont sur la détéction d'erreurs, notament les détections de débordement que nous aurions pu détecter avec la méthode des gardes de mémoire dans les struct fb et db. De plus notre allocateur n'est pas compatible avec valgrind ce qui est un réel problème pour suivre les fuites mémoires.
